//
//  AppDelegate.h
//  Pong2
//
//  Created by Ebba on 2016-02-19.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

