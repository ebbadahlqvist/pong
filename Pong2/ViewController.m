//
//  ViewController.m
//  Pong2
//
//  Created by Ebba on 2016-02-19.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self start];
    
    
    
}

-(void)start{
    
    UIPanGestureRecognizer * pan1 = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(moveObject:)];
    pan1.minimumNumberOfTouches = 1;
    
    [self setUpNewGame];
    [self.racketViewOne addGestureRecognizer:pan1];
    [self playGame];
}



-(void)moveBall{
    [self checkForBounce];
   
    usleep(self.ballSpeed);
    if(self.yNeedsPlus){
        self.y++;
    } else {
        self.y--;
    }
    
    if(self.xNeedsPlus){
        self.x++;
    }  else {
        self.x--;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.ballView.center = CGPointMake(self.x, self.y);
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.pointsLabel setText:[NSString stringWithFormat:@"%d", self.points]];
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.gameOver){
            [self.ballView removeFromSuperview];
            [self.racketViewOne removeFromSuperview];
            sleep(2);
            [self start];
        }
    });
    
    
}

-(void)checkForBounce{
    
    
    if(self.y == self.maxHeight && !self.xNeedsPlus){
        self.yNeedsPlus = NO;
    }
    
    
    if((self.x == 0 && !self.yNeedsPlus)||(self.x == 0 && self.yNeedsPlus)){
        self.xNeedsPlus = YES;
    }
    
       if((self.x == self.maxWidth && !self.yNeedsPlus)|| (self.x == self.maxWidth && self.yNeedsPlus)){
        self.xNeedsPlus = NO;
    }
    
   
    if(self.y == self.maxHeight && self.xNeedsPlus){
        self.yNeedsPlus = NO;
    }
    
    if ((self.y == 0 && self.xNeedsPlus)||(self.y == 0 && !self.xNeedsPlus)){
        self.yNeedsPlus = YES;
    }
    
    
    if(self.y == 0){
        self.points++;
        self.ballSpeed = self.ballSpeed - 1000;
        
        NSLog(@"Points = %d Speed = %d", self.points, self.ballSpeed);
        
        
    }
    
    
    if (self.y == self.maxHeight){
        self.gameOver = YES;
       
    }
    Boolean viewsOverlap = CGRectIntersectsRect(self.ballView.frame, self.racketViewOne.frame);
    
    
    if(viewsOverlap && self.yNeedsPlus){
        self.xNeedsPlus = YES;
        self.yNeedsPlus = NO;
        NSLog(@"KRASCH");
    } else if(viewsOverlap && !self.yNeedsPlus){
        self.xNeedsPlus = YES;
        self.yNeedsPlus = YES;
        NSLog(@"KRASCH");
    
    }
}

-(void)moveObject:(UIPanGestureRecognizer *)pan;
{
    self.racketViewOne.center = [pan locationInView:self.racketViewOne.superview];
    
    
}

-(int)getRandomX{
    
    return arc4random() % self.maxWidth;
}

-(int)getRandomY{
    
    return arc4random() % self.maxHeight;
}

-(void)startAtRandomPlace{
    self.x = [self getRandomX];
    self.y = [self getRandomY];
}

-(void)setUpNewGame{
    self.maxWidth = self.view.frame.size.width;
    self.maxHeight = self.view.frame.size.height;
    
    self.racketViewOne = [[UIView alloc]initWithFrame:CGRectMake(0, 100, 20, 100)];
    self.racketViewOne.backgroundColor = [UIColor redColor];
    
    [self.view addSubview:self.racketViewOne];
    
    self.points = 0;
    
    self.ballView= [[UIView alloc] initWithFrame:CGRectMake([self getRandomX],[self getRandomY],10,10)];
    self.ballView.backgroundColor = [UIColor blackColor];
    
    self.ballSpeed = 13000;
    
    
    [self.view addSubview:self.ballView];
    

}

-(void)playGame{
    
    dispatch_queue_t myQueue =
    dispatch_queue_create("myQueue", nil);
    
    [self startAtRandomPlace];
    self.gameOver = NO;
    
    dispatch_async(myQueue, ^{
        while(!self.gameOver){
            [self moveBall];
        }
    });
    

}

-(void)startTimer {
    self.timer = [NSTimer
                  scheduledTimerWithTimeInterval:4.0
                  target:self
                  selector:@selector(setUpNewGame)
                  userInfo:nil repeats:NO];
}
@end
