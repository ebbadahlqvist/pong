//
//  ViewController.h
//  Pong2
//
//  Created by Ebba on 2016-02-19.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property () int x;
@property () int y;
@property () UIView *ballView;
@property () UIView *racketViewOne;
@property () int maxWidth;
@property () int maxHeight;
@property () BOOL gameOver;
@property () BOOL xNeedsPlus;
@property () IBOutlet UILabel *pointsLabel;
@property () BOOL yNeedsPlus;
@property () int points;
@property () int ballSpeed;
@property (nonatomic) NSTimer *timer;
@end

